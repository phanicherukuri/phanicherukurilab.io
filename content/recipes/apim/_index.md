+++
title = "APIM"
description = "List the child pages of a page"
+++

API management is the process of publishing, promoting and overseeing application programming interfaces (APIs) in a secure, scalable environment. The goal of API management is to allow an organization that publishes an API to monitor the interface’s lifecycle and make sure the needs of developers and applications using the API are being met. API management typically provides the following functions:

* Tools to create and manage API’s
* Provide security, routing and rate limit features
* Real time reports and analytics
* Provide better developer experience through Portal


API management solutions available in the market, in general, can be divided into two major components:

**API Gateway:** API gateway exposes the backend services, such as Web APIs, SOAP APIs, JMS, WebSocket services etc.to the outer world. The key capabilities that it provides include, but are not limited to, the security, transformation, throttling, caching, routing and monitoring. It allows the enterprise to manage the services centrally, de-coupling the backend service providers from end-users and providing ability to have consistent interface across the enterprise.

**API Developer Portal:** It is a self-servicing portal, through which developers can register themselves to gain access to the Web APIs. The primary goal of such a portal is to eliminate human dependency quickly onboard the known and unknown API consumers/developers. Of course, APIs should meet the consumer requirements for whatever product that they are aiming to build. Some of the key capabilities that are accomplished by the portal are API Key Management, Consumer/User Management, Subscription Management, API Discovery, API Lifecycle and API Documentation. API Portal also provides reporting and analytics capabilities which can be used by API consumer, publisher and admin’s.
