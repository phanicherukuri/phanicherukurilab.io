+++
title = "Fast Track Items"
description = "Fast Track Items Related Things"
+++


The main intention of this module is to provide details relating to fast track items like handling CMT process, handling infosec averts which are related to security and handling solution support category.

Useful to understand how infosec works from process perspective.
