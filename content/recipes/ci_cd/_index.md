+++
title = "CI/CD"
description = "CI/CD Related Things"
+++

A CI/CD Pipeline implementation, or Continuous Integration/Continuous Deployment, is the backbone of the modern DevOps environment. It bridges the gap between development and operations teams by automating the building, testing, and deployment of applications. In this blog, we will learn what a CI/CD pipeline is and how it works.

With CI/CD, the more iterations your team can perform every day the better. You can eliminate bottlenecks in your CI/CD pipeline in three main ways
* 3 steps to accelerate CI/CD:
   * Ensure processes work at the right level of abstraction.
   * Eliminate storage capacity and performance constraints.
   * Speed up provisioning.
